<!DOCTYPE html>
<html>
<head>
	<title>Simple Calculator</title>
	<style type="text/css">
	.error {color: #FF0000;}
	</style>
</head>
<body>
<?php

$fn=$sn=$operator="";
$fnErr=$snErr=$opErr="";

if($_SERVER["REQUEST_METHOD"] == "POST")
{
	if(!isset($_POST["fn"]))
		{$fnErr="The first number is required.";}
	else if(!isset($_POST["sn"]))
		{$snErr="The second number is required.";}
	else{
		$fn=test_input($_POST["fn"]);
		$sn=test_input($_POST["sn"]);
	}

	if(empty($_POST["operator"])){
		$opErr="Please select an operation.";
	}
	else{
		$operator=test_input($_POST["operator"]);
	}
}

function test_input($data){
	$data =trim($data);
	$data = stripcslashes($data);
	$data = htmlspecialchars($data);
	return $data;
}

?>




	<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
		First Number: <input type="number" name="fn" value="<?php echo $fn;?>" placeholder="0"/>
		<span class="error">*<?php echo $fnErr;?></span>
		<br>
		
		<input type="radio" name="operator" <?php if (isset($operator) && $operator=="add") echo "checked";?> value="add">+
		<input type="radio" name="operator" <?php if (isset($operator) && $operator=="subtract") echo "checked";?> value="subtract">-
		<input type="radio" name="operator" <?php if (isset($operator) && $operator=="multiply") echo "checked";?> value="multiply">*
		<input type="radio" name="operator" <?php if (isset($operator) && $operator=="divide") echo "checked";?> value="divide">/
		<span class="error">*<?php echo $opErr;?></span>
		<br>
		Second Number: <input type="number" name="sn" value="<?php echo $sn;?>" placeholder="0"/>
		<span class="error">*<?php echo $snErr;?></span>
		<br>
		<button type="submit" name="submit" value="submit">Calculate</button>
	</form>
	<p>The answer is:</p>



	<?php
	if(isset($_POST['submit'])){
		/*if(!isset($_GET['fn'])||!isset($_GET['sn'])){
			echo "<h1>Please input numbers!</h1>";
		}else{*/
		$result1 = $_POST['fn'];
		$result2 = $_POST['sn'];
		$operator = $_POST['operator'];
		
		if($operator=="divide"&&$result2==0){
			echo "ERROR";
		}
		else{
			switch ($operator) {
				case 'add':
				echo $result1+$result2;
				break;
				case 'subtract':
				echo $result1-$result2;
				break;
				case 'multiply':
				echo $result1*$result2;
				break;
				case 'divide':
				echo $result1/$result2;
				break;
				default:
    				echo "Something's wrong!";
				break;
			}
		}
	}
	?>
</body>
</html>
